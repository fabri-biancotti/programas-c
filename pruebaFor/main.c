#include <stdio.h>
#include <stdlib.h>

int main()
{
    char pal []={"Bienvenido"};

    printf("For creciente de un array:\n");

     for(int i=0; i<=sizeof(pal); i++){
        printf("%c",pal[i]);
    }

    printf("\nFor decreciente de un array:\n");

    for(int i=sizeof(pal); i>=0; i--){
        printf("%c",pal[i]);
    }
}
