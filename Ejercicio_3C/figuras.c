#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
#define PI 3.1415926

int main()
{
int figura; float lado, base, h, r, area;
  printf("Calculo de areas:\n");
  printf("Cuadrado:1\n");
  printf("Triangulo:2\n");
  printf("Circunferencia:3\n");
  scanf("%d",&figura);

  switch(figura){
  case 1: {
        printf("Lado:\n");
        scanf("%f",&lado);
        area=pow(lado,4);
        printf("El area del cuadrado es: %f",area);break;
    }
  case 2:{
        printf("Base:\n");
        scanf("%f",&base);
        /*printf("Altura:\n");
        scanf("%f",&h);*/
        h=sqrt(pow(base,2)-pow(base/2,2));
        area=(base*h)/2;
        printf("El area del triangulo es: %f",area);break;
    }
  case 3:{
        printf("Radio:\n");
        scanf("%f",&r);
        area=pow(r,2)*PI;
        printf("El area de la circunferencia es: %f",area);break;
    }
  }
  return 0;
}
